# -*- coding: utf-8 -*-
# server.py
# Fib microservice

from socket import *
from fib import fib
from threading import Thread
from concurrent.futures import ProcessPoolExecutor as Pool

pool = Pool(4)  # создаем пул из 4-х процессов


def fib_server(address):
    sock = socket(AF_INET, SOCK_STREAM)
    sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)  # конфигурируем сетевой порт
    sock.bind(address)  # привязываем к адресу системы
    sock.listen(5)
    while True:
        client, addr = sock.accept()
        print('Connection', addr)
        thread=Thread(target=fib_handler, args= (client,))
        thread.start()


def fib_handler(client):
    while True:
        req = client.recv(100)
        if not req:
            break
        n = int(req)
        future = pool.submit(fib, n)  # передаем задачу в пул
        result = future.result()  # ожидаем результата
        resp = str(result).encode('ascii') + b'\n'
        client.send(resp)
    print('Close client')


fib_server(('localhost', 5001))
