import time
from multiprocessing import Pool
from fib import fib

if __name__ == '__main__':
    start = time.time()
    p = Pool(5)
    print(p.map(fib, [10, 20, 30]))
    end = time.time()
    print(end - start)
