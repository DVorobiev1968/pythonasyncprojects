import time


def fib(n):
    if n<=2:
        return 1
    else:
        return fib(n-1)+fib(n-2)

def main():
    start = time.time()
    fib(10)
    fib(20)
    fib(30)
    end = time.time()
    print(end - start)

if __name__ == '__main__':
    main()
